import { INCREASE, DECREASE } from "../constants/demoConstant";
//action creator
export const increase = () => ({
  type: INCREASE,
});

export const decrease = () => ({
  type: DECREASE,
});
