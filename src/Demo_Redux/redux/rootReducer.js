import { combineReducers } from "redux";
import { demoReducer } from "./reducer/demoReducer";

export const rootReducer_demo = combineReducers({
  demoReducer,
});
