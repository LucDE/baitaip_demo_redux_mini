import { INCREASE, DECREASE } from "../constants/demoConstant";

const initialState = {
  number: 1,
};

export const demoReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case INCREASE: {
      state.number++;
      return { ...state };
    }
    case DECREASE: {
      state.number--;
      return { ...state };
    }
    default:
      return state;
  }
};
