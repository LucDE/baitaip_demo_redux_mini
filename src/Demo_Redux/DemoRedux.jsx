import React, { Component } from "react";
import { connect } from "react-redux";
import { increase, decrease } from "./redux/actions/demoActions";
class DemoRedux extends Component {
  render() {
    let { number, increaseNumber, decreaseNumber } = this.props;
    return (
      <div>
        <h1 className="text-center">{number}</h1>
        <button
          className="btn btn-danger mr-2"
          onClick={() => {
            decreaseNumber();
          }}
        >
          Giảm số lượng
        </button>
        <button
          className="btn btn-success"
          onClick={() => {
            increaseNumber();
          }}
        >
          Tăng số lượng
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    number: state.demoReducer.number,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    increaseNumber: () => {
      dispatch(increase());
    },
    decreaseNumber: () => {
      dispatch(decrease());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoRedux);
