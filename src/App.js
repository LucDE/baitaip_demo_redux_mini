import DemoRedux from "./Demo_Redux/DemoRedux";

function App() {
  return (
    <div className="App d-flex justify-content-center ">
      <DemoRedux />
    </div>
  );
}

export default App;
